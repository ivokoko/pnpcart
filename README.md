# PNP Cart

## Product format:


    <div class="item">
        <div class="name">Apple</div>
        <div>Price: <span class="price">0.25</span></div>
        <div class="pricerow">
            <div><input type="text" value="1" class="qty"/></div>
            <div><a id="action" href="javascript:;">Add to Cart</a></div>
        </div>
    </div>

Add as many as you want.

##To set up promo:
add:

1. data-promo="3for2" // 3 for the price of 2

2. data-pack="3" 3 pack

### This is how the product with promotion looks:

    <div class="item">
        <div class="name">Apple</div>
        <div>Price: <span class="price">0.25</span></div>
        <div class="pricerow">
            <div><input data-pack="3" data-promo="3for2" type="text" value="1" class="qty"/></div>
            <div><a id="action" href="javascript:;">Add to Cart</a></div>
        </div>
    </div>



##Install:

git clone https://ivokoko@bitbucket.org/ivokoko/pnpcart.git

cd pnpcart

npm install

##Run:

node bin/www

go to http://hostname:3000