(function(window,document) {

  loadCart = function() {
    return localStorage['cart'] ? JSON.parse(localStorage['cart']) : [];
  }

  var cart = loadCart();

  calculatePrice = function(obj) {
    return obj.promo !== null?  (obj.price * obj.qty / obj.promo.amount * obj.promo.price).toFixed(2) : (obj.price * obj.qty).toFixed(2);
  }

  printCartContents = function() {

    var cartSum = 0;
    var summaryRow = '';

    var cartHead = '<div><h3>Cart</h3>';
    cartHead += '<table><tr><td>Product</td><td>Qty</td><td>Item Price</td><td>Total Price</td><td></td></tr>';
    var cartText = '<tr><td colspan=\'3\'>Cart is empty</td></tr>';
    var cartEnd = '</table></div>';
    var totaItemPrice = 0;


    if (cart.length > 0) {
      cartText = '';

      cart.forEach(function (obj) {

        totaItemPrice = calculatePrice(obj);
        cartText += '<tr><td>' + obj.name +'</td>';
        cartText += '<td style="text-align: right">' + obj.qty +'</td>';
        cartText += '<td style="text-align: right">' + obj.price.toFixed(2) +'</td>';
        cartText += '<td style="text-align: right">' + totaItemPrice+'</td>';
        cartText += '<td style="text-align: right"><a href="javascript:;" onclick="removeItem(\''+obj.name+'\')">X</a></td></tr>';

        cartSum += parseFloat(totaItemPrice);

      })
      summaryRow = '<tr><td colspan="3" style="text-align: right">Order Total: </td><td style="text-align: right">'+cartSum.toFixed(2)+'</td></tr>';
    }
    document.querySelector('#cart').innerHTML = cartHead+cartText+summaryRow  +cartEnd;

  }

  removeItem = function(product) {
    var index = cart.map(function(obj){ return obj.name; }).indexOf(product);
    if (confirm("Are you sure you want to delete "+ product)){
      cart.splice(index,1);
      printCartContents();
      localStorage['cart'] = JSON.stringify(cart);
    }
  }

  getPromo = function(item) {
    if (item.querySelector('.qty').getAttribute('data-promo')){
      var condition = item.querySelector('.qty').getAttribute('data-promo').split('for');
      return {'amount': condition[0], 'price': condition[1]};
    }else{
      return null;
    }

  }

  warningMessage = function(product) {
    var message = 'Minimum required: '+ product.pack+ '. ';
        message += product.name + ' is sold for packs by '+ product.pack;
    alert(message);
  }

  addToCart = function(product) {
    var index = cart.map(function(obj){ return obj.name; }).indexOf(product.name);
    if (index === -1) {
      if (product.qty > 0 && product.qty >= product.pack && product.qty % product.pack === 0){
        cart.push(product);
      }else{
       warningMessage(product);
      }
    }else{
      if (product.qty > 0 && product.qty >= product.pack && (product.qty +parseInt(cart[index].qty)) % product.pack === 0) {
        cart[index].qty += product.qty;
      }else{
        warningMessage(product);
      }
    }
    printCartContents()
    localStorage['cart'] = JSON.stringify(cart);
  }

  initCart =  function(){
      printCartContents();
      var listItems = document.getElementById('products').childNodes;
      listItems.forEach(function(item){

        if (item.childNodes.length > 0) {
          try {
            item.querySelector('#action').addEventListener('click', function(){
              var product = {name: item.querySelector('.name').innerText,
                             price: parseFloat(item.querySelector('.price').innerText),
                             qty: parseInt(item.querySelector('.qty').value),
                             promo: getPromo(item),
                             pack: item.querySelector('.qty').getAttribute('data-pack') ? parseInt(item.querySelector('.qty').getAttribute('data-pack')) : 1
              }
              addToCart(product);
            })
          }catch(err){
            console.log('Not Valid Product format. Error: '+ err);
          }

        }
      })
    }

    if (window.attachEvent) {
      window.attachEvent('onload', initCart);
    } else {
      window.addEventListener('load', initCart, false);
    }
}(window,document));
